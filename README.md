### vue3+face-api

> 使用方式
```
npm i
npm run dev
```

> 打包
```
npm run build
```

### 注意，models只能存放在public目录下
### 运行在iis服务器可能出现模型无后缀名，导致模型无法加载
### 解决方法，将无后缀名的模型添加后缀名，并在对应的json文件中修改引用
### ps:我已经修改了部分模型的后缀名